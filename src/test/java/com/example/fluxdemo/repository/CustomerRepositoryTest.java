package com.example.fluxdemo.repository;

import com.example.fluxdemo.DBInit;
import com.example.fluxdemo.domain.Customer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.r2dbc.DataR2dbcTest;
import org.springframework.context.annotation.Import;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.function.Predicate;

@DataR2dbcTest
@Import(DBInit.class)
public class CustomerRepositoryTest {

    @Autowired CustomerRepository customerRepository;

    @Test
    public void 한건찾기_테스트() {

        customerRepository.findById(1L).subscribe((c)->{
           System.out.println(c);
        });

        StepVerifier
                .create(customerRepository.findById(1L))
                .expectNextMatches((c) -> {
                    return c.getFirstName().equals("Jack");
                }).expectComplete()
                .verify();

//        StepVerifier
//                .create(customerRepository.findById(1L))
//                .expectNextMatches(new Predicate<Customer>() {
//
//                    @Override
//                    public boolean test(Customer customer) {
//                        return false;
//                    }
//                }).expectComplete()
//                .verify();

    }
}
