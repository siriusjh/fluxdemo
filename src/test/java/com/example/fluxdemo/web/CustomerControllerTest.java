package com.example.fluxdemo.web;

import com.example.fluxdemo.domain.Customer;
import com.example.fluxdemo.repository.CustomerRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import; // interface는 안됨
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.function.Consumer;

import static org.mockito.Mockito.when;

@WebFluxTest

//@SpringBootTest
//@AutoConfigureWebTestClient
public class CustomerControllerTest {

   @Autowired
    private WebTestClient webTestClient; // 비동기로 http 요청

    @MockBean
    //@Autowired
    CustomerRepository customerRepository;

    @Test
    public void 한건찾기_테스트() {
      System.out.println("테스트");

      //given
      Mono<Customer> givenData = Mono.just(new Customer("Jack", "Bauer"));

      // stub -> 행동지시
      when(customerRepository.findById(1L)).thenReturn(givenData);

      webTestClient.get().uri("/customer/{id}", 1L)
              .exchange()
              .expectBody()
              .jsonPath("$.firstName").isEqualTo("Jack")
              .jsonPath("$.lastName").isEqualTo("Bauer");
    }

    
//    @Test
//    public void 한건찾기_테스트2() {
//        Flux<Customer> customer = customerRepository.findAll();
//        customer.subscribe(new Consumer<Customer>() {
//            @Override
//            public void accept(Customer customer) {
//                System.out.println("한건찾기_테스트");
//                System.out.println(customer);
//            }
//        });
//    }
}
