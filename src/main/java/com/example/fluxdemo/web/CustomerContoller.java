package com.example.fluxdemo.web;

import com.example.fluxdemo.domain.Customer;
import com.example.fluxdemo.repository.CustomerRepository;
import org.springframework.http.MediaType;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;

import java.time.Duration;

@RestController
public class CustomerContoller {

    private final CustomerRepository customerRepository;

    //flux 모든 요청의 stream merge, sink맞춰줌, 모든 클라이언트가 접근 가능
    private final Sinks.Many<Customer> sink;

    public CustomerContoller(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
        // multicast : 새로 push된 데이터만
        sink = Sinks.many().multicast().onBackpressureBuffer();
    }

    // 데이터가 모두 조회되면 연결 종료
    @GetMapping(value="/customer", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<Customer> findAll() {
        return customerRepository.findAll().delayElements(Duration.ofSeconds(1)).log();
    }

    // 연결 유지
    // @GetMapping(value="/customer/sse", produces = MediaType.TEXT_EVENT_STREAM_VALUE) // 표준 , sink쓰면 생략
    // 끊으면 재요청이 안됨
    @GetMapping(value="/customer/sse") // sink의 merge 된 데이터 리턴
    public Flux<ServerSentEvent<Customer>> findAllSSE() {
        return sink.asFlux().map(c -> ServerSentEvent.builder(c).build()).doOnCancel(() -> {
            sink.asFlux().blockLast();
            // 마지막 데이터 onComplete 호출 > 재요청 받을 수 있음
        });
    }

    @PostMapping("/customer")
    public Mono<Customer> save() {
        return customerRepository.save(new Customer("tester", "java")).doOnNext(c ->{
            sink.tryEmitNext(c);
            // sink에 push
        });
    }

    // 한건은 mono
    @GetMapping("/customer/{id}")
    public Mono<Customer> findById(@PathVariable Long id) {
        return customerRepository.findById(id).log();
    }

    // 모았다가 5초 후에 한번에 flush
    @GetMapping("/flux")
    public Flux<Integer> flux() {
        return Flux.just(1, 2, 3, 4, 5).delayElements(Duration.ofSeconds(1)).log();
    }

    // 순차적으로 onnext 할때마다
    @GetMapping(value="/fluxstream", produces = MediaType.APPLICATION_STREAM_JSON_VALUE)
    public Flux<Integer> fluxstream() {
        return Flux.just(1, 2, 3, 4, 5).delayElements(Duration.ofSeconds(1)).log();
    }
}
